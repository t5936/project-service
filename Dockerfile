# set base image (host OS)
FROM python:3.8

# set the working directory in the container
WORKDIR /app

# copy the dependencies file to the working directory
COPY requirements.txt .

# install dependencies
RUN pip install -r requirements.txt

# copy the content of the local src directory to the working directory
COPY . .

# Build the project service protos for Python
RUN python -m grpc_tools.protoc \
    --python_out=. \
    --grpc_python_out=. \
    --proto_path=. ./*.proto

ENV HOST=0.0.0.0
ENV PORT=80
ENV DB_URI=mysql+mysqldb://db_user:db_password@mysql-service/db_init

# command to run on container start
EXPOSE 8080
CMD [ "python", "service.py" ]

